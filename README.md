# Dynamic List View Component

## Quick Demo

![Demo1](media/demo1.gif)

## File Overview

### Aura

* **customListViewDisplay** - Main component. Takes an object API name as an input parameter, presents a picklist of that object's listviews, and renders the listview the user selects.

* **customListViewSelector** - Support component for demo purposes. Allows user to pick an object name, then passes it to **customListViewDisplay**

### Static Resource

* **HideActions.css** - Static css we load after rendering that hides the Edit/Delete actions