public with sharing class DynamicListViewController {

    @AuraEnabled
    public static list<ListViewWrapper> ListViewValues(String objectName){
 
        list<ListViewWrapper> result = new list<ListViewWrapper>();
 
        for(ListView eachListView : [SELECT Id, Name, DeveloperName FROM ListView
                                    WHERE sObjectType =:objectName 
                                    ORDER By Name DESC]) { 
            result.add(new ListViewWrapper(eachListView.Name, EachListView.DeveloperName));
        }
        
        return result; 
    }
 
    public class ListViewWrapper{
        @AuraEnabled public String label{get;set;} 
        @AuraEnabled public String developerName{get;set;} 

        public ListViewWrapper(String Name, String devName) {
            this.label = name;
            this.developerName = devName;
        }
    }
}
