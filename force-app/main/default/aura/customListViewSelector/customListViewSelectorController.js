({
    doInit : function(component, event, helper) {
        component.set("v.objectOptions", helper.fetchObjectOptions());
        component.set("v.isObjectSelected", true);
    },
})
