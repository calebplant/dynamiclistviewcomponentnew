({
    doInit : function(component, event, helper) {

    },
    onObjChange : function(component, event, helper) {
        console.log('object changed. New value: ' + event.getParam("value"));
        component.set("v.isListViewSelected", false);
        // call apex method to fetch list view dynamically 
        var action = component.get("c.ListViewValues");
        action.setParams({
            "objectName" : event.getParam("value")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var listViewOptions = response.getReturnValue();
                if(listViewOptions.length > 0){
                    // set listViewOptions attribute with response
                    component.set("v.listViewOptions",listViewOptions);
                    // set first value as default value
                    component.set("v.selectedListView", listViewOptions[0].developerName);
                    // rendere list view on component
                    component.set("v.isListViewSelected", true); 
                    console.log('response from display:');
                    console.log(response.getReturnValue());
                    
                }            } 
            else if (state === "INCOMPLETE") {
                console.log('incomplete...');
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    onListViewSelect : function(component, event, helper) {
        component.set("v.isListViewSelected", false);
        component.set("v.isListViewSelected", true);
    }
})
